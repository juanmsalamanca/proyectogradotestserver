
var entityManager = require('../Entities/entitiesManager.js');

function manageTransaction(request, response){
	console.log('manageTransaction');
	console.log(request.body);
	var service = request.body.service;
	var data =  JSON.parse(request.body.data) ;

	switch(service){
		case "getPagesPermited":
			console.log('paginas permitidas');
			var pages = [];

			var p1 = {name: 'Conductores', url: 'driver.html' };
			var p2 = {name: 'Vehiculos', url: 'vehicle.html' };
			pages.push(p1);
			pages.push(p2);
			var data = {pages: pages};

			serviceResponse(response, 'application/json', 'success', data);
		break;

		case "save-entity":
			entityManager.save(data);
					

			serviceResponse(response, 'application/json', 'success', data);
		break;

		default:
		console.log('Service not define: ' + service);

	}

}

function serviceResponse(response, contenType, result, data){
	var obj = {
		result: result,
		data: data
	};
	response.setHeader('Content-Type', contenType);
    response.send(obj);
    response.end();
}

exports.manageTransaction = manageTransaction;