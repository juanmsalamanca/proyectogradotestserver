/**
 * Created by juan on 14/09/2016.
 */
var mysql=require('mysql');

function MySqlConnection(obj){
    var connection=mysql.createConnection({
        host: obj.host,
        user: obj.user,
        password: obj.password,
        database: obj.database
    });
    
    this.executeQuery = function (query){
        connection.connect(function(err) {
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            } else {

                try{
                    console.log("Query: " + query);
                    connection.query(query, function(err, rows, fields) {
                        if (err) throw err;
                        console.log(rows);
                        connection.destroy();
                    });
                }catch (error){
                    console.log("Error: " + error)
                }


            }
        });
    }
    
    this.testConnection = function () {
        connection.connect(function(err) {
            console.log('connected as id ' + connection.threadId);
            if (err) {
                console.error('error connecting: ' + err.stack);
                return;
            } else {
                console.log("Data Base test ok");
                connection.query('SHOW databases', function(err, rows, fields) {
                    if (err) throw err;
                    for (var p in rows) {
                        console.log('DataBase: ', rows[p]["Database"]);
                    }
                    connection.destroy();
                });
            }
        });
    }
}

exports.connection = {
    host:'54.213.81.66',
    user:'user',
    password:'0000',
    database:'test'
}
connectionToTest = {
    host:'54.213.81.66',
    user:'user',
    password:'0000',
    database:'test'
}
connectionProduction = {
    host:'54.213.81.66',
    user:'user',
    password:'0000',
    database:'production'
}
exports.MySqlConnection = MySqlConnection;
