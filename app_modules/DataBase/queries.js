
exports.getInsertQ =  function(tableName, entity){
	var query = "INSERT INTO "+ tableName;
	var columns = " (";
	var values = " (";
	var iterator = new IteratorObj(entity);
	while(iterator.next()){
		columns +=  iterator.getKey()  ;
		values += "'" +iterator.getValue() + "'";
		if(iterator.getIndex() < iterator.getLength() - 1){
			columns += ", ";
			values += ", ";
		}
	}
	query += columns + ") VALUES " + values + ");";
	return query;
}

function IteratorObj(obj){
	var index = -1;
	var array = [];

	if(obj != undefined){
		for(var attr in obj){
			array .push(attr);
		}
	}else{
		console.log('OBJECT UNDEFINED');
	}

	this.next = function(){
		index++;
		return index < array.length ? true : false;
	}
	this.getValue = function(){
		return obj[array[index]];
	}
	this.getKey = function(){
		return array[index];
	}
	this.getIndex = function () {
		return index;
	}
	this.getLength = function(){
		return array.length;
	}

}