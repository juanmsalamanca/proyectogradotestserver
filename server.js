/**
 * Created by juan on 14/09/2016.
 */
var express = require('express');
var router = express.Router();
var db = require('./app_modules/DataBase/Db');
var transactionManger = require('./app_modules/Manager/TransactionManager');
var myParser = require("body-parser");

var app=express();

app.use(myParser.json({extended : true}));
app.use(myParser.urlencoded({extended: true}));
app.use(router);

app.use(express.static(__dirname + '/public'));

app.listen(8080,function(){
    console.log('Servidor web iniciado');
});

var test = new db.MySqlConnection(db.connection);
setTimeout(function () {
   test.testConnection();
}, 1000);

app.post("/Transaction", function(request, response) {
    console.log('Transaction');
    transactionManger.manageTransaction(request, response);    	
});




/////////////////////////////





router.get('/test', function(req, res, next) {
	var obj = {
		data1: 45,
		data2: "data"
	};
	var json = JSON.stringify(obj);

	res.setHeader('Content-Type', 'application/json');
   // res.send("rest api response");
    res.send(json);
    res.end();
});


 app.post("/testpjson", function(request, response) {
    console.log(request.body); 
      
	var obj = {
		data1: 45,
		data2: "okpost"
	};
	var json = JSON.stringify(obj);

	response.setHeader('Content-Type', 'application/json');
    response.send(json);
    response.end();
});

  


