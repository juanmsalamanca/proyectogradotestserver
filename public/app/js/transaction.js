function validateResult(obj){
	if(obj.result === 'success'){
		return true;
	}else{
		return false;
	}
}

function to_JsonString(obj){
	var jsonObject = {};
	for(var prop in obj){
		if(obj[prop] != ""){
		 jsonObject[prop] = obj[prop];
		}		
	}
	return JSON.stringify(jsonObject);
}

function sendPost(url, service, dataJson, callback){
	$.ajax({
		url : url,
		type : 'post',
		data : jQuery.param({
			data: to_JsonString(dataJson),	
			service: service
		}),
		beforeSend: console.log("beforeSend"),
		success : function(data) {				
			callback(dataJson, data);
		},
		error: function (jqXHR, exception){	
			console.log('jqXHR: ' + jqXHR  ); 
			console.log('exception: ' + exception);
			callback(dataJson, "");
		 }	
	});	
}

function getExternalHtml(url){
	var htmlCode;
	$.ajax({
		url : url,
		async: false,
		type : 'get',
		processData : false,
		success : function(data) {				
			htmlCode = data;
		},
		error: function (jqXHR, exception){	
			console.log('jqXHR: ' + jqXHR  ); 
			console.log('exception: ' + exception);			
		 }	
	});	
	return htmlCode;
}
function innerHtmlPage(url, idToPlace){
	var htmlPageCode = getExternalHtml(url);
	document.getElementById(idToPlace).innerHTML = htmlPageCode;
}