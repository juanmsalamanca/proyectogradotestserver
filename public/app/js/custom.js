function togglePanel(idDiv){
	if(document.getElementById(idDiv).querySelector(".panel-body").style.display === "none"){		
		$("#"+idDiv+ " .fa ").first().removeClass("fa-chevron-down");
		$("#"+idDiv+ " .fa ").first().addClass("fa-chevron-up");
	}else{		
		$("#"+idDiv+ " .fa ").first().removeClass("fa-chevron-up");
		$("#"+idDiv+ " .fa ").first().addClass("fa-chevron-down");
	}
	$("#"+idDiv+ " .panel-body ").first().slideToggle("fast");
}